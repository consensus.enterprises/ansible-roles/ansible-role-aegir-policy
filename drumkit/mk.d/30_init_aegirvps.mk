AEGIRVPS_GROUP_VARS_TEMPLATE ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/aegirvps-group-vars.yml.j2
AEGIRVPS_HOST_VARS_TEMPLATE  ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/localhost.yml.j2
AEGIRVPS_GROUPS_PLAYBOOK_TEMPLATE   ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/aegirvps-group-playbook.yml.j2
AEGIRVPS_ADMIN_USERS_HOST_VARS_FILE ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/admin-users-host-vars.yml.j2

init-role-aegirvps-intro:
	@echo "Setting up Consensus AegirVPS project (consensus.aegir-policy)."
# Overrides to add volumes and mounts.
init-role-aegirvps: OPENSTACK_HOST_VARS_TEMPLATE:=$(AEGIRVPS_HOST_VARS_TEMPLATE)
init-role-aegirvps: AEGIR_GROUP_VARS_TEMPLATE:=$(AEGIRVPS_GROUP_VARS_TEMPLATE)
# Overrides to add admin users.
init-role-aegirvps: AEGIR_GROUPS_PLAYBOOK_TEMPLATE:=$(AEGIRVPS_GROUPS_PLAYBOOK_TEMPLATE)
init-role-aegirvps: init-role-aegirvps-intro init-role-aegir init-role-openstack init-admin-users-host-vars
	@echo "Finished setting up Consensus AegirVPS project (consensus.aegir-policy)."

init-admin-users-host-vars-intro:
	@echo Deploying consensus.admin-users host vars file.
init-admin-users-host-vars: init-admin-users-host-vars-intro
	@make -s ansible-add-host-vars-file ANSIBLE_HOST_VARS_TEMPLATE=$(AEGIRVPS_ADMIN_USERS_HOST_VARS_FILE) ANSIBLE_HOST_VARS_FILENAME=all.yml
	@echo Finished deploying consensus.admin-users host vars file.

clean-role-aegirvps-intro:
	@echo Cleaning up aegir vps playbooks and vars files.
clean-role-aegirvps: clean-role-aegirvps-intro ansible-clean-host ansible-clean-groupo clean-role-openstack
	@echo Finished cleaning up aegir vps playbooks and vars files.
# TODO: Also remove git submodules.
