upgrade-aegir: ## Upgrade Aegir to the latest code available on the currently-deployed branch
	@make -s groups extra_vars="aegir_force_upgrade=true"
