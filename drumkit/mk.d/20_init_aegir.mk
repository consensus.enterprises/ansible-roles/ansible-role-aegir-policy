AEGIR_SUBMODULES_CONFIG_FILE   ?= $(AEGIR_POLICY_ROLE_DIR)/drumkit/submodules.aegir
AEGIR_GROUPS_PLAYBOOK_TEMPLATE ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/aegir-group-playbook.yml.j2
AEGIR_GROUP_VARS_TEMPLATE      ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/aegir-group-vars.yml.j2
AEGIR_HOST_VARS_TEMPLATE       ?= $(AEGIR_POLICY_ROLE_DIR)/files/templates/aegir-host-vars.yml.j2

# Initialize aegir_submodules hash
$(call initialize_submodules_hash,$(AEGIR_SUBMODULES_CONFIG_FILE),aegir_submodules)

aegir_submodule_paths=$(call keys,aegir_submodules)
$(aegir_submodule_paths):
	@echo Submoduling $@
	@git submodule add $(call get,aegir_submodules,$@) $@

init-role-aegir-intro:
	@echo "Setting up Aegir project (consensus.aegir-policy)." 
init-role-aegir: ANSIBLE_GROUPS_PLAYBOOK_TEMPLATE=$(AEGIR_GROUPS_PLAYBOOK_TEMPLATE)
init-role-aegir: ANSIBLE_GROUP_VARS_TEMPLATE=$(AEGIR_GROUP_VARS_TEMPLATE)
init-role-aegir: ANSIBLE_HOST_VARS_TEMPLATE=$(AEGIR_HOST_VARS_TEMPLATE)
init-role-aegir: init-role-aegir-intro init-project-ansible $(aegir_submodule_paths)
	@echo "Finished setting up Aegir project (consensus.aegir-policy)." 

clean-role-aegir-intro:
	@echo Cleaning up Aegir playbooks and vars files.
clean-role-aegir: clean-role-aegir-intro ansible-clean-host ansible-clean-group
	@echo Finished cleaning up Aegir playbooks and vars files.
# TODO: Also remove git submodules.
