Consensus: Aegir Policy [![pipeline status](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/badges/master/pipeline.svg)](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/commits/master)
=======================

Consensus Enterprises' Aegir policy role.

Overview
--------

This role represents a set of sensible defaults for [the Aegir hosting system](https://www.aegirproject.org/) as deployed via the Consensus Aegir role, [consensus.aegir](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir).

In general, this set of defaults represents our policy when deploying Aegir
internally at Consensus; by invoking this role out of the box, you can deploy Aegir according to our standards.

Alternately, you can use this role as a baseline, and then selectively override its defaults to customize your own Aegir further.

Requirements
------------

Same as [consensus.aegir](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir).

Example Playbooks
-----------------

Example playbook for deploying Aegir according to our standard policy:

```yaml
---
- hosts: localhost
  remote_user: root
  roles:
    - ansible-role-aegir-policy
```

Example playbook for deploying Aegir and overriding 2 defaults:

  * Deploy on Apache instead of Nginx.
  * Supply an alternate admin email.

```yaml
---
- hosts: localhost
  remote_user: root

  vars:
    aegir_policy_overrides:
      - aegir_http_service_type: apache
      - aegir_admin_email: do-not-reply@mydomain.com

  roles:
    - ansible-role-aegir-policy
```

Note: this could also be accomplished using [group and host vars](https://docs.ansible.com/ansible/latest/user_guide/playbooks_best_practices.html#group-and-host-variables) to customize particular Aegir hosts/groups as needed.

Role Variables: aegir_policy_defaults
-------------------------------------

The main functionality of consensus.aegir-policy is delivered via the
`aegir_policy_defaults` [role variable](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/blob/master/defaults/main.yml). It is a list of sensible default values to be applied to
the [variables in the consensus.aegir role](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir/blob/master/defaults/main.yml) and [the other roles we use to deploy MySQL, Nginx, and PHP](#dependencies). See [consensus.aegir's documentation](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir) for details.

Other Role Variables
--------------------

Role variables other than `aegir_policy_defaults` 
are listed below, along with their default values; see also 
[defaults/main.yml](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/blob/master/defaults/main.yml):

```yaml
aegir_policy_branch: consensus-stable
```

By default, we deploy Aegir and its subcomponents (Provision, Drush, etc.) [from source](https://gitlab.com/consensus.enterprises/aegir); this variable indicates what branch to use.

```yaml
aegir_policy_roles:
  - consensus.mysql
  - geerlingguy.nginx
  - consensus.php
  - consensus.aegir
```

The list of roles to invoke, in order, when deploying Aegir.

```yaml
aegir_perform_legacy_cleanup: false
```

Whether to run scripts that clean up unused/deprecated components when redeploying Aegir on legacy servers.

```yaml
bind_mounts:
  '/var/aegir/backups':
    src: '/opt/var/aegir/backups'
    owner: aegir
    group: aegir
  '/var/lib/mysql':
    src: '/opt/var/lib/mysql'
```

Default set of bind mounts to create for Aegir backups and the MySQL database;
this is done via the [consensus.utils](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-utils) role, a subdependency of consensus.aegir. Set `bind_mounts: {}` to skip this step and have `/var/aegir/backups` and `/var/lib/mysql` on the root partition.

```yaml
print_aegir_login_link: True
```

Whether to print a login link (via `drush @hm uli`) for the Aegir front-end after deploying.

```yaml
use_msmtp: True
deploy_msmtp_config: True
msmtp_domain: "localhost"
```

Default configuration for the MSMTP MTA to be used by Aegir.

Dependencies
------------

In its default configuration, this role invokes the following roles, in order:

  - [consensus.mysql](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-mysql)
  - [geerlingguy.nginx](https://github.com/geerlingguy/ansible-role-nginx)
  - [consensus.php](https://github.com/consensus-enterprises/ansible-role-php)
  - [consensus.aegir](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir)

Development and Testing
-----------------------

This role is built using [Drumkit](https://drumk.it), and includes 
[tests](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/tree/master/features), written in [Behat](https://behat.org), for use in [our Gitlab CI environment](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/-/jobs). See also Drumkit's [documentation on test support](https://drumk.it).

Issue Tracking
--------------

For bugs, feature requests, etc., please visit the [issue tracker](https://gitlab.com/consensus.enterprises/ansible-roles/ansible-role-aegir-policy/-/boards).

License
-------

GNU AGPLv3

Author Information
------------------

Written by [Christopher Gervais](https://consensus.enterprises/team/christopher/) and [Dan Friedman](https://consensus.enterprises/team/dan/). To contact us, please use our [Web contact form](https://consensus.enterprises/#contact).
